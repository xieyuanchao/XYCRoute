//
//  NSError+XYCRouteError.m
//  WLRRoute_Example
//
//  Created by xyc on 2018/2/7.
//  Copyright © 2018年 Neo. All rights reserved.
//

#import "NSError+XYCRouteError.h"

NSString * const XYCRouteErrorDomain = @"www.xycroute.com";

@implementation NSError (XYCRouteError)


+(NSError *)XYCRouteNotFoundError{
    return [self XYCRouteErrorWithCode:XYCRouteErrorNotFound msg:@"The passed URL does not match a registered route."];
}
+(NSError *)XYCRouteTransitionError{
    
    return [self XYCRouteErrorWithCode:XYCRouteErrorHandlerTargetOrSourceViewControllerNotSpecified msg:@"TargetViewController or SourceViewController not correct"];
}
+(NSError *)XYCRouteHandleBlockNoTeturnRequest
{
    return [self XYCRouteErrorWithCode:XYCRouteErrorBlockHandleNoReturnRequest msg:@"Block handle no turn WLRRouteRequest object"];
}

+(NSError *)XYCRouteMiddlewareRaiseErrorWithMsg:(NSString *)error{
    return [self XYCRouteErrorWithCode:XYCRouteErrorMiddlewareRaiseError msg:[NSString stringWithFormat:@"WLRRouteMiddle raise a error:%@",error]];
}
+(NSError *)XYCRouteErrorWithCode:(NSInteger)code msg:(NSString *)msg{
    NSDictionary *userInfo = @{ NSLocalizedDescriptionKey: NSLocalizedString(msg, nil) };
    return [NSError errorWithDomain:XYCRouteErrorDomain code:code userInfo:userInfo];
}

@end
