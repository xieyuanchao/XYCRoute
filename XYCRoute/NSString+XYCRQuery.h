//
//  NSString+XYCQuery.h
//  WLRRoute_Example
//
//  从NSString中获取出"?"后面的key／value键值对
//
//  Created by xyc on 2018/2/6.
//  Copyright © 2018年 Neo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (XYCRQuery)

- (NSDictionary *)XYCRParametersFromQueryString ;

@end
