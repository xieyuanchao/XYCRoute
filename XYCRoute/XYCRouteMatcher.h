//
//  XYCRouteMatcher.h
//  WLRRoute_Example
//
//  Created by xyc on 2018/2/6.
//  Copyright © 2018年 Neo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XYCRouteRequest.h"

@interface XYCRouteMatcher : NSObject

//原始的传入的url表达式。tjobqy://qy.alizhaopin.com/detail   ,  /signin/:phone([0-9]+)  ,  /userDetail  等
@property(nonatomic,copy)NSString * originalRouteExpression;

//去掉了schame之后的部分，qy.alizhaopin.com/detail   ,  /signin/:phone([0-9]+)  ,  /userDetail  等
@property(nonatomic,copy)NSString * routeExpressionPattern;


+(instancetype)matcherWithRouteExpression:(NSString *)expression;
-(XYCRouteRequest *)createRequestWithURL:(NSURL *)URL primitiveParameters:(NSDictionary *)primitiveParameters;

@end
