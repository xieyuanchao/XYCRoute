//
//  XYCMatchResult.h
//  WLRRoute_Example
//
//  Created by xyc on 2018/2/6.
//  Copyright © 2018年 Neo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XYCMatchResult : NSObject

//是否匹配上url
@property (nonatomic, assign, getter=isMatch) BOOL match;

/**
 If matched,the paramProperties dictionary  will store url's path keyword paramaters away.
 如果匹配成功，url路径中的关键字对应的值将存储在该字典中.
 
 url参数的健值对 /signin/:phone([0-9]+)/:phone([0-9]+)       WLRDemo://com.wlrroute.demo/signin/13812345432/xyc123456   读取出来的是：phone 13812345432;  password xyc123456（一般只有一个，有多个参数的话，直接用‘?’后面的键值对来代替）
 
 显示在注册的时候取出key按照先后顺序保存起来，然后在调用的地方取出value，也按照先后数据存入对应key的value中去
 */
@property (nonatomic, strong) NSDictionary *paramProperties;

@end
