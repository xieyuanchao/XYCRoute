//
//  XYCRouteRequest.h
//  WLRRoute_Example
//
//  Created by xyc on 2018/2/6.
//  Copyright © 2018年 Neo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XYCRouteRequest : NSObject

@property (nonatomic, copy, readonly) NSURL *URL;
@property(nonatomic,copy)NSString * routeExpression;
@property (nonatomic, copy, readonly) NSDictionary *queryParameters;//url’?‘后面的键值对
@property (nonatomic, copy, readonly) NSDictionary *routeParameters;//url参数的健值对 /signin/:phone([0-9]+)/:password([0-9]+)       WLRDemo://com.wlrroute.demo/signin/13812345432/xyc123456   读取出来的是：phone 13812345432;  password xyc123456（一般情况下后面只会有一个参数，多个参数的话，可以使用“?”后面的键值对来替代）
@property (nonatomic, copy, readonly) NSDictionary *primitiveParams;//自定义参数键值对


/**
 初始化request

 @param URL 实际的请求地址
 @param routeExpression 需要匹配的正则表达式，如："/signin/:phone([0-9]+)"，在注册handler的时候会传入到matcher中去
 @return XYCRouteRequest实例
 */
-(instancetype)initWithURL:(NSURL *)URL routeExpression:(NSString *)routeExpression routeParameters:(NSDictionary *)routeParameters primitiveParameters:(NSDictionary *)primitiveParameters;

/**
 通过中括号直接从三个dict中获取对应key的value
 */
- (id)objectForKeyedSubscript:(NSString *)key;

@end
