//
//  XYCRouter.h
//  WLRRoute_Example
//
//  Created by xyc on 2018/2/6.
//  Copyright © 2018年 Neo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XYCRouteHandler.h"

@interface XYCRouter : NSObject

/**
 单例

 @return XYCRouter
 */
+ ( instancetype ) sharedInstance;

//注册一个handler
-(void)registerHandler:(XYCRouteHandler*)handler forRoute:(NSString*)route;

/**
 处理页面跳转请求

 @param URL 目标页面的URL
 @param primitiveParameters 自定义参数键值对
 @param completionBlock 调用完成后的回调，成功失败都在这里
 @return 调用成功／失败
 */
-(BOOL)handleURL:(NSURL *)URL primitiveParameters:(NSDictionary *)primitiveParameters completionBlock:(void(^)(BOOL handled, NSError *error))completionBlock;

@end
