//
//  NSError+XYCRouteError.h
//  WLRRoute_Example
//
//  Created by xyc on 2018/2/7.
//  Copyright © 2018年 Neo. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, WLRError) {
    
    /** The passed URL does not match a registered route. */
    XYCRouteErrorNotFound = 45150,
    
    /** The matched route handler does not specify a target view controller. */
    XYCRouteErrorHandlerTargetOrSourceViewControllerNotSpecified = 45151,
    XYCRouteErrorBlockHandleNoReturnRequest = 45152,
    XYCRouteErrorMiddlewareRaiseError = 45153
};

@interface NSError (XYCRouteError)

+(NSError *)XYCRouteNotFoundError;
+(NSError *)XYCRouteTransitionError;
+(NSError *)XYCRouteHandleBlockNoTeturnRequest;
+(NSError *)XYCRouteMiddlewareRaiseErrorWithMsg:(NSString *)error;
@end
