//
//  UIViewController+XYCRoute.h
//  WLRRoute_Example
//
//  为所有vc都添加一个XYCRouteRequest变量，在新的vc中就能读取传入的参数了
//
//  Created by xyc on 2018/2/7.
//  Copyright © 2018年 Neo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XYCRouteRequest.h"

@interface UIViewController (XYCRoute)

@property(nonatomic,strong)XYCRouteRequest * routeRequest;

@end
