//
//  UIViewController+XYCRoute.m
//  WLRRoute_Example
//
//  Created by xyc on 2018/2/7.
//  Copyright © 2018年 Neo. All rights reserved.
//

#import "UIViewController+XYCRoute.h"
#import <objc/runtime.h>

@implementation UIViewController (XYCRoute)

-(XYCRouteRequest *)routeRequest{
    XYCRouteRequest * dict = objc_getAssociatedObject(self, "routeRequest");
    return dict;
}
-(void)setRouteRequest:(XYCRouteRequest *)routeRequest{
    
    objc_setAssociatedObject(self, "routeRequest", routeRequest, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
