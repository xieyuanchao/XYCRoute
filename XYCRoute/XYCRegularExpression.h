//
//  XYCRegularExpression.h
//  WLRRoute_Example
//
//  这是一个正则表达式的匹配类，用户注册的时候会生成正则表达式，完了在调用的以后会进行匹配，匹配上了就可以处理这个请求，否则就不能处理这个请求
//
//  Created by xyc on 2018/2/6.
//  Copyright © 2018年 Neo. All rights reserved.
//

#import <Foundation/Foundation.h>

@class XYCMatchResult;

@interface XYCRegularExpression : NSRegularExpression

/**
 对string进行匹配判断，匹配上的话，需要将url‘／’后面的参数取出来保存起来
 @return 匹配结果
 */
-(XYCMatchResult *)matchResultForString:(NSString *)string;

/**
 注册handler的时候传入的规则string

 @param pattern 去掉了schema的urlstring
 @return XYCRegularExpression实例
 */
+(XYCRegularExpression *)expressionWithPattern:(NSString *)pattern;

@end
