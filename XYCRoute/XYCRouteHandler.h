//
//  XYCRouteHandler.h
//  WLRRoute_Example
//
//  Created by xyc on 2018/2/7.
//  Copyright © 2018年 Neo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XYCRouteRequest.h"
#import <UIKit/UIKit.h>


/**
 获取sourcevc，targetvc，然后执行跳转
 注意：用户需要重写targetViewControllerWithRequest方法，传递目标vc给route，这样route就能执行跳转了
 */
@interface XYCRouteHandler : NSObject

//- (BOOL)shouldHandleWithRequest:(XYCRouteRequest *)request;

/**
 获取目标vc
 */
-(UIViewController *)targetViewControllerWithRequest:(XYCRouteRequest *)request;

/**
 获取跳转用的vc，navivc可以push，普通vc可以present，默认取用的事rootvc
 */
-(UIViewController *)sourceViewControllerForTransitionWithRequest:(XYCRouteRequest *)request;

/**
 处理请求
 */
-(BOOL)handleRequest:(XYCRouteRequest *)request error:(NSError *__autoreleasing *)error;

/**
 执行页面跳转动作
 */
-(BOOL)transitionWithWithRequest:(XYCRouteRequest *)request sourceViewController:(UIViewController *)sourceViewController targetViewController:(UIViewController *)targetViewController error:(NSError *__autoreleasing *)error;

@end
