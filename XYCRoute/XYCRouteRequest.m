//
//  XYCRouteRequest.m
//  WLRRoute_Example
//
//  Created by xyc on 2018/2/6.
//  Copyright © 2018年 Neo. All rights reserved.
//

#import "XYCRouteRequest.h"
#import "NSString+XYCRQuery.h"

@implementation XYCRouteRequest

-(instancetype)initWithURL:(NSURL *)URL routeExpression:(NSString *)routeExpression routeParameters:(NSDictionary *)routeParameters primitiveParameters:(NSDictionary *)primitiveParameters{
    
    if (!URL) {
        return nil;
    }
    self = [super init];
    if (self) {
        _URL = URL;
        _queryParameters = [[_URL query] XYCRParametersFromQueryString];
        _routeExpression = routeExpression;
        _routeParameters = routeParameters;
        _primitiveParams = primitiveParameters;
    }
    return self;
}

-(id)objectForKeyedSubscript:(NSString *)key{
    id value = self.routeParameters[key];
    if (!value) {
        value = self.queryParameters[key];
    }
    if (!value) {
        value = self.primitiveParams[key];
    }
    return value;
}

@end
